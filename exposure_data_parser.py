import argparse
import pandas
import tkinter
import matplotlib.pyplot as plt
from datetime import timedelta
import datetime

parser = argparse.ArgumentParser(description='Tool to poke around in exposure data exports')

parser.add_argument('input_file', type=str, help='input json file')

args = parser.parse_args()

f = open(args.input_file)

data = pandas.read_json(f)

if data['matchesCount'].max()>0:
    print("A match was detected")
else:
    print("No matches detected")



t_exposure_time = timedelta(minutes = float(data['keyCount'].values.sum() * 15))

print("Number of years of exposure checked against: {}".format(t_exposure_time.days / 365.25))

# data['timestamp'] = pandas.to_datetime(data['timestamp'])
# data.set_index('timestamp')
# plt.plot(data['timestamp'], data['keyCount'])
# plt.plot(data['timestamp'], data.rolling(14).mean()['keyCount'])
# plt.show()
#import pdb; pdb.set_trace()
print(data[data['matchesCount']!=0][['timestamp', 'matchesCount', 'hash']])
